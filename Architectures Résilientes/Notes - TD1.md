# Notes - TD1

- Pour voir les évènements DHCP: *debug ip dhcp server events*

## 2. Image Docker

Ajouter la ligne:

RUN apt-get --assume-yes install iputils-ping

## 3. Interfacage de GNS3

Une fois ajoute le container docker, on va sur Edit > Advanced > Additional directories...

/etc

/var

## 4. Routage simple

N'oubliez pas de mettre la commande *no shutdown* lors de la configuration de l'interface f0/0

## 8. Filtrage entre vlans par access-list

Dans ce chapitre, il faut definir tout d'abord le mot de passe pour l'utilisateur "sr07" :

```bash
sr07# passwd sr07
```

et deux lignes dans le fichier /etc/ssh/*sshd_config* avant de démarrer le service sshd (optionnel)

```bash
PermitRootLogin yes
PasswordAuthentication yes
```

On peut continuer avec le demarrage du service sshd.

Après l'application de l'access-list INFRA_OUT sur l'interface e1/2.10, on a testé le ping depuis le conteneurs :

- serv-pers(1) -> poste-pers(2) : pas de ping
- poste-pers -> serv-pers : Packet filtered

Car il y a une regle implicite de refuser toute autre traffic sauf celui autorise par l'ACL.

Ensuite, la permission de pinguer :

```bash
R1# permit icmp host 10.0.20.3 host 10.0.10.3
```

Comme résultat de la tentative de connexion ssh, on a un refus sur la console du routeur:

```bash
list INFRA_OUT denied tcp 10.0.20.3 -> 10.0.10.3
```

Afin de résoudre ce problème, on ajoute les lignes suivantes :

```bash
R1# permit tcp host 10.0.20.3 host 10.0.10.3 eq 22
R1# no xx (où xx est la ligne de l'instruction "deny ip any any log"
```

Après, l'erreur de *apt-get install locate* est donnée à cause d'un problème sur la résolution du nom de domaine (DNS). Donc il faut ajouter un access-list **réflexive**.

> Le but de ces ACL est de **faire attention au traffic**, dans ce cas, celui de l'entrée de l'interface e1/2.10 (c’est à dire pour le traffic en provenance du conteneur debian-sr07-1 "serv-pers" vers le routeur ou l’extérieur). Ensuite, le router va recevoir les paquets et il va les permettre, les rappeler et créer des access-list dynamiques.
> 

Par exemple, on va créer l'access-list INFRA_IN qui permet le trafic sous le protocole *UDP* à travers du port 53, et on va **refléter le traffic** vers la liste DNSREQ. Ensuite, sur la liste INFRA_OUT, on ajoute la ligne *evaluate DNSREQ.* 

Alors, on permet le traffic depuis la machine *serv-pers* vers 195.83.155.55 (traffic en provenance du *serv-pers* vers le routeur ou l'extérieur) mais lors de chaque envoie de paquet qui satisfasse les conditions de l'ACL INFRA_IN, on va enregistrer cette demande sur l'access-list DNSREQ.

Donc, si on n'a jamais fait un requete depuis *serv-pers* vers l'exterieur, le traffic entrant va être bloqué car on a la règle *evaluate DNSREQ* dans l'ACL INFRA_OUT. Mais si on a déjà fait une premier demande, on va avoir des règles sur DNSREQ et le traffic entrant va être permis.

Voici l'ajout de la règle pour permettre le traffic UDP vers le port 53 pour la communication DNS :

```bash
R# conf t
R(config)# ip access-list extended INFRA_IN
R(config-ext-nacl)# permit udp host 10.0.10.3 host 195.83.155.55 eq 53 reflect DNSREQ
R(config-ext-nacl)# deny ip any any log
R(config-ext-nacl)# exit
R(config)# int e1/2.10
R(config-if)# ip access-group INFRA_IN in
R(config-if)# exit
R(config)# ip access-list extended INFRA_OUT
R(config-ext-nacl)# 24 evaluate DNSREQ
```

Après cette configuration, le résultat de la commande “*apt-get install locate*” donne un refus de connexion :

```bash
list INFRA_IN denied tcp 10.0.10.3(51332) -> 195.83.155.55(3128)
```

Pour résoudre cette problème, on ajoute une entrée réflexive de debian-sr07-1 vers 195.83.155.55 sur le port 3128 en tcp :

```bash
R# conf t
R(config)# ip access-list extended INFRA_IN
R(config-ext-nacl)# permit tcp host 10.0.10.3 host 195.83.155.55 eq 3128 reflect PROXYWEB
R(config-ext-nacl)# no 20 (deny ip any any log)
R(config-ext-nacl)# exit
R(config)# ip access-list extended INFRA_OUT
R(config-ext-nacl)# evaluate PROXYWEB
```

On a le résultat suivant :

![access-lists.png](img_td1/access-lists.png)

Finalement, le protocole UDP est bloqué sur les ports 67 et 68 pour les requêtes DHCP. On ajoute un règle dans **INFRA_IN** permettant de débloquer les requêtes :

```bash
permit udp any any eq bootpc (port 68 du client)
permit udp any any eq bootps (port 67 du serveur)
```

PS: les ACL sont lues de haut en bas, donc il faut faire attention à la règle *"deny ip any any log"* parce que toute ligne en dessous de cette règle n'aura aucun effet.

## 9.Lien lacp

Ajouter un *no shutdown* sur chaque interface (port-channel 1, f2/0 y f2/1)

## 10. Interconnexion des 2 topologies

### a) VLANs

Correction :

port 0: VLAN 50 access (pour ens19)

1. infra1 : VLAN 50 access
2. infra2 : VLAN 50 access
3. serv-estu : VLAN 40 access
4. poste-estu : VLAN 40 access
5. serv-labo : VLAN 30 access
6. poste-labo : VLAN 30 access
7. R3: VLAN 1 dot1q

```bash
auto eth0
iface eth0 inet static
	hwaddress ether 2e:00:00:00:50:03 
	address 10.0.50.3
	netmask 255.255.255.0
	gateway 10.0.50.1
	up echo nameserver 195.83.155.55 > /etc/resolv.conf
```

### Création des interfaces vlans sur R2

```bash
R1# conf t
R1 (config)# int f0/0
R1 (config-if)# no shutdown
R1 (config-if)# int f0/0.30
R1 (config-if)# encapsulation dot1Q 30
R1 (config-if)# ip address 10.0.30.1 255.255.255.0 
R1 (config-if)# no shutdown
R1 (config-if)# int f0/0.40
R1 (config-if)# encapsulation dot1Q 40
R1 (config-if)# ip address 10.0.40.1 255.255.255.0 
R1 (config-if)# no shutdown
R1 (config-if)# int f0/0.50
R1 (config-if)# encapsulation dot1Q 50
R1 (config-if)# ip address 10.0.50.1 255.255.255.0 
R1 (config-if)# no shutdown
R1 (config-if)# end
R1 # write mem
```

Correction: le ping est depuis le routeur **R1** sur 10.0.50.1.

### b) DHCP

Correction: **option** au lieu de options. **domain-name-servers** au lieu de dns...

- Dans la subnet 10.0.50.0, on a mis la machine sr07-gns3 avec l'adresse .100
- Ajout du serveur dhcp dans la vlan 50 (port 0 du Switch2, mode access).

### Configuration finale :

**Serveur DHCP**

- #ip route
    
    ![ip route.png](img_td1/ip_route.png)
    
- #cat /etc/default/isc-dhcp-server
    
    ![isc-dhcp-server.png](img_td1/isc-dhcp-server.png)
    
- #cat /etc/network/interfaces
    
    ![interfaces.png](img_td1/interfaces.png)
    
- #cat /etc/dhcp/dhcpd.conf
    
    ![dhconf.png](img_td1/dhconf.png)
    

**SR07-GNS3**

- VLANs du Switch2
    
    ![vlans switch.png](img_td1/vlans_switch.png)
    
- R2# show run
    
    ![sh run R2.png](img_td1/sh_run_R2.png)
    
    ![sh run R2 2.png](img_td1/sh_run_R2_2.png)
    

**Resultats**

- Demande de DHCP depuis "poste-labo"
    
    ![DHCP CLient.png](img_td1/DHCP_Client.png)
    
- Response du serveur DHCP (#tail -f /var/log/syslog)
    
    ![DHCP Server.png](img_td1/DHCP_Server.png)
    

Finalement, pour la configuration **apache2,** on doit tout d'abord supprimer les proxy's ajoutes dans la config initial du conteneur: (sur les 2 serveurs et les 2 clients)

```bash
# unset http_proxy
# unset https_proxy
```

Pour supprimer l'affichage *apache2: Could not reliablydetermine the server's...*

```bash
# nano /etc/apache2/apache2.conf
ServerName xxxx
```

### c) Access-lists

1. Il y aura des autres clients apres ? Si oui, ces nouveaux clients auront la possibilite de pinger ?
    
    Sinon, ca suffit d'ajouter "permit icmp any any"
    
2. permit tcp host 10.0.30.4 host 10.0.30.3 eq 80 (www)