## Technologies utilisées dans l'UV

### Module “Architectures résilientes”

- Serveur DHCP (isc-dhcp-server) ou sur le routeur
- Conteneurs Docker
- Logiciel GNS3 (simulation de réseaux)
    - Interfaces
    - Persistance
    - Routage
    - VLAN (protocole 802.1q)
    - NAT
    - Access-lists
    - LACP (protocole 802.3ad)
    - Authentification 802.1x
    - Freeradius
- Nextcloud
- Serveur NAS/SAN redondant (RAID)
- NFS
- LVM
- Snapshots
- iSCSI
- Serveur LDAP
- MariaDB
- NginX

### Module “Défense d’un SI”

- tcpdump
- wireshark
- NIDS (snort)
    - Règles snort
- Vulnérabilités par canaux auxiliaires
    - Caches CPU
    - Exécution 'out-of-order' et prédiction de branchement dans les CPU
    - Exploitation du cache comme canal auxiliaire
    - Vulnérabilité Spectre
- Tests d’intrusion
    - Injection de commande
    - Contournement de filtrage IP
    - Local File Inclusion
    - JSON Web Token (JWT)
    - XSS
    - Nessus
- Envoi des logs
    - Filebeat
    - Logstash
    - ElasticSearch
    - Kibana
- OpenVAS
