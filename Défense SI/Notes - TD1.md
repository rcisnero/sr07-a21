# Notes - TD1

## Introduction

Le TD peut être fait sur une machine virtuelle sur Proxmox ou sur ta propre machine sur Virtualbox. Donc il peut avoir plusieurs interfaces (entre autres):

- **eth0** - Accès à internet via NAT sur VirtualBox.
- **lo** - Interface virtuelle loopback pour simuler un réseau (localhost).
- **ens18** - Accès à internet via UTC sur Proxmox.

Alors, il faut utiliser l'interface selon ton cas. 

## 1. Obtention des traces

Pour décompresser le fichier :

```bash
$ gunzip newdat3.log.gz
```

Ensuite, on vérifie l'empreinte MD5. 

> Pour rappel, MD5 est un algorithme de hachage généralement utilisé pour vérifier l'intégrité des données. Si ces données sont modifiées (même de seulement un bit) alors le hash MD5 sera complètement différent. Voilà pourquoi MD5 est utilisé pour vérifier l'intégrité des données et/ou pour reconnaître un fichier (car le nom de fichier n'est pas utilisé pour calculer la signature). Plus d'info sur [https://fr.wikipedia.org/wiki/MD5](https://fr.wikipedia.org/wiki/MD5)
> 

Un hash MD5 est généralement représenté par une chaine hexadécimale de 32 caractères (dans le cas de ce TD : 38e85119953076c904fd2105dfcb6cdb).

`md5sum newdat3.log`

`file newdata3.log.gz`

`file newdata3.log`

1. tcpdump

Entrer comme root

`tcpdump -i ens18 -w capture`

`tcpdump -r capture`

Pour relire le fichier newdata, on le rajoute l'extension .pcap et on execute:

`tcpdump -r newdata.log.pcap`

1. wireshark (capture /relire /filtre)

`[tcp.stream](http://tcp.stream) eq 6`

1. snort (capture /relire /regles)

`snort -i ens18 -v`

1. NIDS

`action protocole adresse_source port_source opérateur_de_direction adresse_destination port_destination options`

`log tcp !192.168.1.0/24 any <> 192.168.1.0/24 23`

elle va logger n'importe quel port de l'extérieur de notre réseau vers le traffic telnet de notre réseau

`alert tcp !192.168.1.0/24 any -> 192.168.1.0/24 111 (content: "|00 01 86 a5|"; msg: "external mountd access";)`

`alert tcp any any -> any any (flags:S; msg:"SYN packet"; sid:1000006; rev:1;)`

flag syncrhonise

Correction :

1. creer le traffic (interface lo pour le loopback)

`sudo tcpdump -i eth0 -w capture-ping.pcap`

1. Faire des ping
2. appliquer la règle snort

`sudo snort -r capture-ping.pcap -c snort-icmp.regles -l log -A full`

avec -r, on utilise le tcpdump file

1. mode NIDS

`sudo snort -r newdata.log -c /etc/snort/snort.conf —-`

`fgrep '[**]' log-snort/alert | cut -d ' ' -f 3- | cut -d '[' -f 1 | sort | unique`

1. wireshark

[`tcp.stream](http://tcp.stream) eq 9`

voir la ligne 585 - 

1. tcpflow

`mkdir flows`

`cd flows`

`tcpflow -r ../newdata3.log`

`file * | grep ASCII | cut -d ':' -f1`

`for F in 'file * | grep ASCII | cut -d ':' -f1' ; do echo "=== $F ====" ; cat $F; done | more`