# TD.D1 Détection d'intrusion

## But du TD

Le but de ce TD est d'analyser une (vieille) attaque survenue sur un pot de miel. Vous devez déterminer l'ampleur de l'attaque et la faille exploitée. Attention, il y a urgence, le pirate a peut-être pénétré vos systèmes...

## Obtention des traces
- Télécharger l'archive newdat3.log.gz
- Décompresser le fichier
- Vérifier son empreinte md5 : 38e85119953076c904fd2105dfcb6cdb

## Tcpdump
- Installer tcpdump
- Utiliser tcpdump pour faire une capture des paquets sur votre interface réseau et la sauvegarder dans un fichier.
- Utiliser tcpdump pour lire le fichier newdat3.log.

## Wireshark
- Installer wireshark
- Utiliser wireshark pour faire une capture de paquets sur votre interface réseau et la sauvegarder dans un fichier.
Ouvrir le fichier newdat3.log avec wireshark.
- Utiliser quelques filtres pour avoir une idée du type de flux.

## Snort
- Installer snort
- Utiliser snort pour faire une capture de paquets sur votre interface réseau.
- Explorer les options de snort pour obtenir plus ou moins de détails sur les paquets capturés.
- Comprendre l'écriture d'une règle snort
` action protocole adresse_source port_source opérateur_de_direction adresse_destination port_destination options`
    - "Action" désigne l'action que snort devra réaliser lorsqu'un paquet correspondant à la règle aura été détecté :
        - alert : génère une alerte et logue le paquet
        - log : logue le paquet
        - pass : ignore le paquet
        - activate : alerte et active une règle dynamique
        - drop : indique à iptables de jeter le paquet, logue le paquet
        - reject : indique à iptables de jeter le paquet, logue le paquet, envoi TCP reset ou ICMP port unreachable selon le protocole de niveau 4 utilisé
        - sdrop : indique à iptables de jeter le paquet mais ne le logue pas

    - "Protocole" désigne : TCP, UDP, IP, ICMP
    - Les adresses sont des adresses IP v4 au format CIDR. Par exemple, 192.168.1.0/24 signifie : toute adresse depuis 192.168.1.1 jusqu'à 192.168.1.255. Il est possible de spécifier des ensemble d'adresses en utilisant des crochets et la virgule en séparateur (ex : [192.168.1.0/24,10.1.1.0/24]). Il est possible d'utiliser un opérateur de négation : en ajoutant "!" devant l'adresse ou l'ensemble d'adresse, snort sélectionnera le paquet uniquement s'il vient d'une autre adresse que celles spécifiées.
    - Les ports sont donnés sous la forme d'un entier ou d'un intervalle, avec la syntaxe "début:fin" (ex 1:1024).
    - Les opérateurs de directions sont "->" (source vers destination) et "<>" (source vers destination et destination vers source).
    - Les options permettent de préciser les paquets à sélectionner. Elles sont listées entre parenthèses, séparées par des points-virgules. Elles utilisent des mots-clés. Elles se répartissent en 4 catégories :
        - general (informations à propos de la règle elle-même), 
        - payload (recherche d'information dans le payload du paquet), 
        - non-payload (recherche d'information ailleurs que dans le payload),
        - post-detection (comportement une fois que la règle est vérifiée). Voici quelques mots-clés :
            - msg : affiche un message
            - reference : affiche une URL pour obtenir de l'information sur l'alerte.
            - gid : indique quelle partie de snort est concerné par cette règle.
            - sid : identifiant de la règle. Pour une règle 'maison', utiliser un sid supérieur à 1000000.
            - rev : numéro de révision de la règle.
            - classtype : permet de classer la règle par catégorie (eg. attempted-admin, shellcode-detect, trojan-activity...)
            - priority : priorité de l'alerte
            - content : recherche un texte, ou du binaire si la chaine est entre des caractères "|".

- Déterminer le rôle des règles suivantes :
` log tcp !192.168.1.0/24 any <> 192.168.1.0/24 23` 
` alert tcp !192.168.1.0/24 any -> 192.168.1.0/24 111 (content: "|00 01 86 a5|"; msg: "external mountd access";)
alert tcp any any -> any any (flags:S; msg:"SYN packet"; sid:1000006; rev:1;)`

## Ecriture de règles snort
- Ecrire dans un fichier une règle snort qui affiche "PING !" pour détecter les ping sur votre machine.
- Lancer snort en mode NIDS sur une interface réseau en lui indiquant ce fichier de règle et en lui demandant de loguer dans un répertoire les alertes et les paquets.
- Créer un trafic ping sur l'interface réseau surveillée par snort. Vérifier que les alertes sont bien générées.
- Même question pour le trafic web.
- Exploiter le mode NIDS de snort pour analyser newdate3.log
- Lancer snort en mode NIDS avec les règles par défaut (cf. /etc/snort/snort.conf) sur le fichier newdate3.log
- En utilisant des commandes shell, classer les noms des alertes (messages fournis par le mot clé "msg") par ordre alphabétique, en enlevant les doubles.
- En utilisant des commandes shell, retrouver la règle correspondante dans le répertoire des règles snort.

## Analyse de l'attaque avec wireshark
- Dans wireshark, appliquer un filtre sur le protocole correspondant au flux déterminé précédemment.
- En recherchant le mot-clé "string" avec "Ctrl F", vous devriez retrouver le paquet 418.
- En utilisant "Suivre le flot" dans le menu apparaissant avec le clic droit, analyser les échanges qui se sont produits durant la session incriminée.

## Utilisation de tcpflow
- Installer tcpflow
- Créer un répertoire spécifique, eg. FLOWS.
- Appliquer tcpflow au fichier newdate3.log de sorte que les flux TCP soient stockés dans le répertoire FLOWS.
- Examiner les flux obtenus. Utiliser la commande "file" pour déterminer leurs types. Cette commande peut-elle se tromper quant à la nature de certains fichiers ?
- En utilisant le shell, afficher le contenu des fichiers de flux qui sont de type texte. Indice : utiliser for, file, grep, cut, xargs
- Analyser ce contenu.
- Explorer ces fichiers compressés. Attention, il s'agit de (vieux) outils d'attaques et vous ne devriez pas les conserver après ce TD.
