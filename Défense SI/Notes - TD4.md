# Notes - TD4

## 1. Préparation de 2 VMS

- VM Logstash avec 4 Go de RAM
- interface ens18 au lieu de enp0s8
- Habiliter ssh en modifiant `/etc/ssh/sshd_config`, puis `“PermitRootLogin yes”`

### 3.2 Activation et démarrage du service elasticsearch

J’ai aussi configuré :

- cluster.name
- node.name
- discovery.type = single-node

Plus d’info ([https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-discovery-settings.html](https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-discovery-settings.html))

Créer un fichier dans le repertoire `/etc/elasticsearch/jvm.options.d/<nom_du_fichier>`

Ici, on met les deux xms et xmx

### 3.3 Création d’un Index-Template sur ElasticSearch:

Commande corrigé :

`curl -X PUT -H "Content-Type: application/json" "[http://localhost:9200/_index_template/filebeat-geoip](http://localhost:9200/_index_template/filebeat-geoip)" -d "@filebeat-geoip.template"`

## 4. Installation de Logstash sur la VM Logstash

Faire attention à la conf qui soit bien écrite

## 5. Installation de Filebeat sur la VM Nginx:

Faire attention à mettre chaque ligne dans sa section (inputs dans inputs, output dans output)

More info :

[https://www.elastic.co/guide/en/beats/filebeat/current/configuring-howto-filebeat.html](https://www.elastic.co/guide/en/beats/filebeat/current/configuring-howto-filebeat.html)

## 6. Test d'envoi des logs

Si on n’a pas de fichier sr07.log, arretez et vérifiez la configuration de logstash et filebeat

### 7.2. Visualisation des logs :

On doit remplacer la première adresse (s/...) par l’adresse qui existe déjà sur notre fichier access.log, et pour la deuxième un peut ajouter quelques sauts pour simuler une autre adresse

`tail -1 /var/log/nginx/access.log | sed 's/172.30.213.210/172.30.213.100/g' >> /tmp/sr07.log`

## 8. SNMP

snmpwalk -v2c -c public localhost .1.3.6.1.4.1.2021.11

# Notes

- Augmenter le timeout de elasticsearch lors de démarrage :

```bash
# mkdir /etc/systemd/system/elasticsearch.service.d
# echo -e "[Service]\nTimeoutStartSec=180" | tee /etc/systemd/system/elasticsearch.service.d/startup-timeout.conf
# systemctl daemon-reload 
# systemctl show elasticsearch.service | grep ^Timeout
TimeoutStartUSec=3min
TimeoutStopUSec=infinity
# systemctl start elasticsearch
```

## OpenVAS

`docker run -d -p 443:443 -e PUBLIC_HOSTNAME=172.23.4.159 --name openvas mikesplain/openvas`

More info : [https://github.com/mikesplain/openvas-docker](https://github.com/mikesplain/openvas-docker)