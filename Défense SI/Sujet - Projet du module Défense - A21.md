# Sujet du projet du module Défense - automne 2021

Le projet du module Défense consiste à :

- Terminer la récupération des logs Nginx sur Logstash (TD.D4 ELK)
- Rendre opérationnels les dashboards correspondants aux accès Nginx (TD.D4 ELK)
- Installer fprobe pour envoyer les flux réseaux au format Netflow v9 depuis la machine Nginx vers Logstash
- Récupérer les flux Netflow v9 sur le Logstash
- Créer des dashboards pour analyser les flux Netflow v9 (Top 10 talkers sources, Top 10 destinations, Top 10 des conversations, Top 10 des protocoles...)
- Installer Openvas sur une 3ème VM et scanner les vulnérabilités potentielles de la machine Nginx
- Grâce à fprobe les scans sont censés être stockés par ElasticSearch. Mettre en évidence, dans Kibana un scan lancé par Openvas sur Nginx.

### Bonus :
- Modifier la configuration de la machine virtuelle Nginx de sorte que Openvas relève des alertes.
- Analyser les alertes obtenues.